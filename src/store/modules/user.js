/*
 * @Author: 鲨鱼辣椒 1330019012@qq.com
 * @Date: 2022-05-19 22:47:55
 * @LastEditors: 鲨鱼辣椒 1330019012@qq.com
 * @LastEditTime: 2022-05-19 23:05:47
 * @FilePath: \vue-admin-template\src\store\modules\user.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { getToken } from "@/utils/auth";
import { getLoginInfo } from "@/api/login";
const getDefaultState = () => {
  return {
    token: getToken(),
    userInfo: null,
  };
};

// 测试
const state = getDefaultState();

const mutations = {
  SET_USERINFO: (state, data) => {
    state.userInfo = data;
  },
};

const actions = {
  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getLoginInfo()
        .then((res) => {
          console.log(res.data);
          commit("SET_USERINFO", res.data);
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  // user logout
  logout({ commit, state }) {
    console.log(3);
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
